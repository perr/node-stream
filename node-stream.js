/*

SOCKETS:
	- You can fetch socket.io.js from /socket.io/socket.io.js
	- Bind Socket.io to /
	- Emit "subscribe", "event_name"

HTTP CONTROL
	- POST to /event_name with JSON data in data param

*/

var express = require('express');
var app = express();
app.use(express.bodyParser());
var server = require('http').createServer(app)
   ,io = require('socket.io').listen(server, {origins:"*:*"})
   ,events = require('events');

app.use('/html', express.static(__dirname + '/html'));

server.listen(3042);

app.post('/:channel', function(req, res) {
	console.log(req.body);
	var data = req.body.data;
	var chan = req.params.channel;
	event_delegator.emit(chan, data);
	res.send("OK!"+data+chan);
	// console.log("OK!"+data+chan);
});

var event_delegator = new events.EventEmitter;
io.sockets.on('connection', function(socket){
	var subscriptions = {};
	socket.on('subscribe', function(id) {
		if (subscriptions[id]){
			socket.emit("status", {"ok":false, "message":"already subscribed to "+id});
		} else {
			subscriptions[id] = function (data) {
				socket.emit(id, JSON.parse(data));
			};
			event_delegator.on(id, subscriptions[id]);
			socket.emit("status", {"ok":true, "message":"subscribed to "+id});
		}
	});
	socket.on('unsubscribe', function(id) {
		if(subscriptions[id]){
			event_delegator.removeListener(id, subscriptions[id]);
			delete subscriptions[id];
			socket.emit("status", {"ok":true, "message":"unsubscribed from "+id});
		} else {
			socket.emit("status", {"ok":false, "message":"no subscription to "+id});
		}
	});
});

